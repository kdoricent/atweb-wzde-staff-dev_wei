:Program

// globals
// -----------------
// a - Overlays to use {screen overlay, printer overlay} with @Gi
// c - current child
// d - current second name line
// e - current email
// f - current file
// i - current image
// r - current image reduced to 400X320X24
// s - current sort
//     |0 - 0 = all,   1 = new, 2 = printed
//     |1 - {sort,up/down} |0 - 0 = time,  1 = name 2 = email/employee
//                         |1 - 0 = Ascending value, 1 = Descending value  (reversed for time sort)
//
// Dlls
// -----------------
// i = imagetool.dll
//
//  LISTSETS
// -----------------
//  0 - |0 - FILENAME
//      |1 - DATETIME LIST
//      |2 - 0 = NEW 1=OLD
//      |3 - 0 = UNSELECTED 1 = SELECTED
//  1 - {LIST OF SELECTED THINGS BEING PRINTED}
//  2 - {LIST OF TEMP IMAGE PATHS}
//
// Files
// -----------------
// Uiicons.mas


// ----------------------------------------------------------------------------
:Code 0 0 0
[@("ReprintPrintBadge top: ",@LE)["c:\temp\trap.txt"[@IF]@Ow]@(@RT$6%3@[:2Z]@CA," : ",)@Pc@Cw]
[{b,c,d,e,f}@("PrintBadge globals: ",@LE)["c:\temp\trap.txt"[@IF]@Ow]@(@RT$6%3@[:2Z]@CA," : ",)@Pc@Cw]
IF{@Zl>0 `@Zm,@Zc'@[@(,"=",@EV," ")]@CA@Zu;1@Zi}
@IU
@OW@3,
@{"Icons\Circle-Screen.bmp","Icons\Circle-Printer.bmp"}@[@PD@SO@Pi]^a,
{"M",0,1}^M~|0@9
DO{@6
   M@{|1,|2}@IF{@& @8},
   @RK=28 @HK@IF{|0="M" ^M;
                 |0="B" ^B;
                 |0="F" ^F};
   @LK=29 @HK@IF{|0="M" M~|0@9;
                 @{|0="B",B|1@{,@T0@R0$3="No "@NV}@&}@& B@IF{@{|1=0,|2=0}@& @29,
                                                                            @16;
                                                               |2=1 {2,{1}}@9;
                                                               |2=2 {2,{0}}@9;
                                                               |2=3 {2,{2}}@9;
                                                               |1-1@T0@18,""^B};
                 @{|0="F",|1=0,@F0@R0$3="No "@NV}@& @20;
                 @{|0="F",|1=1}@& @26};
   @LK},
 @30,
 a@[@Ci]@DU@CW;


// ----------------------------------------------------------------------------
:Code not used 1 1
;


// ----------------------------------------------------------------------------
:Code LoadIcons 2 2
{"Icon_CheckboxUnselected","uiicons.mas"@PD}@XF@Pi,
{"Icon_CheckboxSelected","uiicons.mas"@PD}@XF@Pi;


// ----------------------------------------------------------------------------
:Code Init 3 3
`600,700'@Iw,
"Reprint Badge"@Ih,
{"Georgia",500,"","","","",""}@If,
0@Ik,
""@Wb@Ww@Wh,
{1,{0,0}}^s@2,
""@5@8@6@RK,
"imagetool.dll"@PD@Xi,
"";

// ------------------------------------- Footer ---------------------------------

// ----------------------------------------------------------------------------
:List Footer 4 4
{Print|Queue Old}


// ----------------------------------------------------------------------------
:Code DisplayFooter 5 5
^A,@4@[@IF{A @Bw}]@Ef@Db@Ah@Ts@Wf;


// ------------------------------------- Header ---------------------------------


// ----------------------------------------------------------------------------
:Code DisplayHeader 6 6
@V0@IF{@NV 0^S^N;
       @[@{@IF{|2=0 N+1^N},
       @IF{|3=1 S+1^S}}]}
@("New - ",N+0,"  Selected - ",S+0)@Wh;


// ----------------------------------------------------------------------------
:List Menu 7 7
{{Show All|Show New|Show Printed}|{Sort By Time|Sort By Name|Sort By Email}}


// ----------------------------------------------------------------------------
:Code DisplayMenu - Argument is Pointer to Button 8 8
@7@{|0@[@IF{@NV "";@{@HV,s|0}@= @Bw}]@Ef@Db@Av," ",
    |1@[@IF{@NV "";@{@HV,s|1|0}@= @Bw}]@Ef@Db@Av}@Av@Ts@Wm;


// ------------------------------------- Body ------------------------------------


// ----------------------------------------------------------------------------
:Code UpClickMenu 9 9
@IF{|0=0 @{|1,s|1}^s;
    |0=2 @{s|0,|1@{|0,@IF{@{|0,s|1|0}@= s|1|1+1\2;0}}}^s},
@10@[|1@IF{|3@NV @{,@IF{|2=0 1;0}}@JV}],
@U0@16;


// ----------------------------------------------------------------------------
:Code RebuildList 10 10
s|0@IF{=1 @11;
       =2 @12;
       1 {@11,@12}@MV}
@{s|1,}@IF{|0|0=0 |1@13;
           |0|0=1 |1@14;
           |1@15};


// ----------------------------------------------------------------------------
:Code ListNew 11 11
"\cache\*.cdb"@PD@Qf@[@{,0}@JV];


// ----------------------------------------------------------------------------
:Code ListPrinted 12 12
"\Printed\*.cdb"@PD@Qf@[@{,1}@JV];


// ----------------------------------------------------------------------------
:Code TimeSort 13 13
@[@{@{@{2,|3}@-,|2,|0@UA},@{|0,|2,|3}}]@IF{s|1|1=0 @DV;@UV};


// ----------------------------------------------------------------------------
:Code NameSort 14 14
@[@{@{|3,|0@UA,|2},@{|0,|2,|3}}]@IF{s|1|1=0 @UV;@DV};


// The email isn't stored in the working value List Set, so it is extracted from |0 (filename)
// ----------------------------------------------------------------------------
:Code EmailSort    15 15
@[@{@{|3,|0@{,#";"+1}@~$@{,#";"}@UA,|2},@{|0,|2,|3}}]@IF{s|1|1=0 @UV;@DV};


// ----------------------------------------------------------------------------
:Code ShowFiles 16 16
IF{@L0@N0@H0=0 {("No",
                 s|0@IF{=1 " New";
                        =2 " Printed";
                        1 ""}," Images")}@U0@[@Br];
   @17}@Ts@Dd@Ed@Am@Wb;


// ----------------------------------------------------------------------------
:Code DisplayList 17 17
{{"","Name","Date","Email"}@Sg,
 @V0@[@{@{|3@Gi,
          |0@{,#";"^L}@$@Al,|1@[:2Z]@(|1,"/",|2," ",|3,":",|4)@Ar,
 // Extract the email from the file name
          |0@{,L+1}@~$@{,#";"}@$@Al},|2}
          @IF{|1=0 |0@By;1 |0@Br}]}@IV;


// ----------------------------------------------------------------------------
:Code SelectBadge 18 18
@T0@O0,IF{@L0@R0=1 0;1}@W0@C0@16;


// ------------------------------------ Printing ---------------------------------


// ----------------------------------------------------------------------------
:Code Print 19 19
[@("19 top: ",@LE)["c:\temp\trap.txt"[@IF]@Ow]@(@RT$6%3@[:2Z]@CA," : ",)@Pc@Cw]
@IF{{("Badge for ",c," etc."),1,"Portrait","Default"}
    [@("pre @OP: ",@LE)["c:\temp\trap.txt"[@IF]@Ow]@(@RT$6%3@[:2Z]@CA," : ",)@Pc@Cw]
    @OP
    [@("post @OP: ",@LE)["c:\temp\trap.txt"[@IF]@Ow]@(@RT$6%3@[:2Z]@CA," : ",)@Pc@Cw]
    @NV;
    @PP,@CP,
// move files
    @25,
// cleanup temp files
    @30};


//  list 0 has {filename,date,old/new,selected/unselected}
//  so, lets print the OLDEST selected files:Code 20
// ----------------------------------------------------------------------------
:Code PrintMultiple 20 20
// what is the max number of images to print on one page
6
// GetOldestSelectedFiles
@22[@U1]
// setup images
@[@IF{ @23}]
// format on page
@{
// this must be writing the preview screen
  @{a|1,}@21@Wb
  ""@Wf@Wm,
// this is sending to printer
  @{"",}@21@19}
// upclick menu???
@9,
""@5;


// ----------------------------------------------------------------------------
:Code Format 21 21
//  Set up the screen and printer icons in local variable A
[|0^A,
[@("A: ",@LE)["c:\temp\trap.txt"[@IF]@Ow]@(@RT$6%3@[:2Z]@CA," : ",)@Pc@Cw]
//  B is just space between each column ONLY 2 WORKS
  "  "^B,]
//  C is path of the banner image
//  For each icon on the list, apply the correct overlay
//  for each one {path of picture cast as object,overlay,firstName,lastName}
//  To make sure spacing is correct even if less than a full sheet, a blank jpg will be used of the same dimensions
  |1@[
      [@("21: ",@LE)["c:\temp\trap.txt"[@IF]@Ow]@(@RT$6%3@[:2Z]@CA," : ",)@Pc@Cw]
      @{@{"",
// logo image
          IF{|0 "FlowerRubiksCubePrint.jpg"@PD@SO;
             "FlowerRubiksCubePrintBlank.jpg"@PD@SO},
// name
          @{@{|2,|3}@[@(,B)]@Av,
// this seems like some kind of way to overlay the name
            {45,50}@IO}},
//
          |0@IF{ @{,A@Gi}}
          }]
[@("21a: ",@LE)["c:\temp\trap.txt"[@IF]@Ow]@(@RT$6%3@[:2Z]@CA," : ",)@Pc@Cw]
// take each pair and arrange into rows of three
@MV@{3,}@GV
//  For each child arrange a row of {banner, space, face}.
//  Now arrange {image, col spacer, image, col spacer, image}
 @[@{"blank_column_spacer.jpg"@PD@SO@{},
     |0,"blank_column_spacer.jpg"@PD@SO@{},
     |1,"blank_column_spacer.jpg"@PD@SO@{},
     |2,"blank_column_spacer.jpg"@PD@SO@{}}]
[@("21b: ",@LE)["c:\temp\trap.txt"[@IF]@Ow]@(@RT$6%3@[:2Z]@CA," : ",)@Pc@Cw]
@[@{,"blank_row_spacer.jpg"@PD@SO@{}}]@MV~%1
[@("21c: ",@LE)["c:\temp\trap.txt"[@IF]@Ow]@(@RT$6%3@[:2Z]@CA," : ",)@Pc@Cw]
@Am@Bw;


//  pass in the # of files to return
//  PAD with ""s if there aren't enough selected
:Code GetOldestSelectedFiles 22 22
@{@{@V0@[@IF{|3=0 "";@{|1,}}]@PV@UV@[|1],@XV@[""]}@MV,}@$;


//  {filename,date,old/new,selected/unselected} on the stack
//
//
//  RETURNS
//  |0 =  path of bitmap to print (in print Temp directory, object cast
//  |1 =  overlay
//  |2 =  firstName
//  |3 =  lastName
//
// ----------------------------------------------------------------------------
:Code SetupImage 23 23
@{@(@IF{|2^N=0 "\cache\";|2=1 "\Printed\"},|0)@PD^F@Or,
  DO{@Gl @IF{|0="EMAIL" |2^E;
             |0="CHILD" @{|2^C^c,|3^D^d};
             |0="OVERLAY" |2^V;
             |0="IMAGE" |2^I,IF{N=0 "cache\";"Printed\"}@(,I)@PD^I}}
  @Cr
  I@iO,
// ******************** maybe shrink size here?
  @{@{"Print Temp\temp",|0,".",|1@CA,".bmp"}@CA@PD^R[@IF{@F2@Y2;@J2}],275,275,24}@iB
  }
// |0 -
// |1 -
// |2 - First Name
// |3 - Last Name
{R@SO,V,@27@Tm,@28@Tm};


// ----------------------------------------------------------------------------
:Code MakeBold 24 24
@{@{"A"@PI@PO,}@Ah,
  @{"AA"@PI@PO,}@Ah,
  @{"AAA"@PI@PO,}@Ah,
  @{"AAAA"@PI@PO,}@Ah,
  @{"AAAAA"@PI@PO,}@Ah};


// ----------------------------------------------------------------------------
//  move file from New folder to Printed folder
//  Selected files are in V1,
:Code MoveFiles 25 25
@V1@[@IF{|2=0@NV;
         @("\cache\",|0)@PD^f@Or,
         DO{@Gl @IF{|0="IMAGE" @("cache\",|2)@PD^i}},
         @Cr,
         f@{,~#"\"}@%^F,
         i@{,~#"\"}@%^I,
         {f,("printed\",F)@PD}@RF,
         {i,("printed\",I)@PD}@RF,
         f@DF,i@DF}],
@NL@U1;


// ----------------------------------------------------------------------------
:Code QueueOld 26 26
//  copy any OLD & SELECTED files from the Printed folder back to the Cache folder
//  coppied files are then deleted from the Printed folder.
@F0,DO{@M0 @K0@IF{@{|2,|3,1}@= |0@{@("\Printed\",),@("\cache\",)}@[@PD]@{@{|0,|1}@{@CF,
                                                                                   // need to also ove the bitmap
                                                                                   @[~%4]@CF},
                                                                         |0@{@DF,~%4@DF}}}},
@10@[|1@IF{|3@NV @{,@IF{|2=0 1;0}}@JV}],
@U0@16;


// ----------------------------------------------------------------------------
:Code FirstName 27 27
c;


// ----------------------------------------------------------------------------
:Code LastName 28 28
d;


// ----------------------------------------------------------------------------
:Code ProcessMassChecks 29 29
@V0@IF{@[|3]@{@{,0}@#,@CV}@= @[@{|0,|1,|2,0}];
       @[@{|0,|1,|2,1}]}@U0;


// ----------------------------------------------------------------------------
:Code CleanupTempFiles 30 30
@V2@[@DF,""];


// ----------------------------------------------------------------------------
:Code debug 31 31
@{"debugReprintBadge.txt"@PD@IF{@QF@NV @IF}@Ow,@{,@RT}@LE@Pc,@Cw};


